package me.frenchgrandpa.planetgame.util;

public class Vector {
	
	private double x = 0;
	private double y = 0;
	
	public Vector() {
	}

	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	
	//getters and setters
	public void setX(double x) {
		this.x = x;
	}
	
	public double getX() {
		return x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getY() {
		return y;
	}
	
	public void setLength(double length) {
		double angle = getAngle();
		x = Math.cos(angle) * length;
		y = Math.sin(angle) * length;
	}
	
	public double getLength() {
		return Math.sqrt(x * x + y * y);
	}
	
	public void setAngle(double angle) {
		double length = getLength();
		x = Math.cos(angle) * length;
		y = Math.sin(angle) * length;
	}
	
	public double getAngle() {
		return Math.atan2(y, x);
	}
	
	
	/////////////
	//operators//
	/////////////
	
	//add
	public void add(double value) {
		x += value;
		y += value;
	}
	
	public void add(double x, double y) {
		this.x += x;
		this.y += y;
	}
	
	public void add(Vector vec) {
		x += vec.getX();
		y += vec.getY();
	}
	
	
	//subtract
	public void sub(double value) {
		x -= value;
		y -= value;
	}
	
	public void sub(double x, double y) {
		this.x -= x;
		this.y -= y;
	}
	
	public void sub(Vector vec) {
		x -= vec.getX();
		y -= vec.getY();
	}
	
}
