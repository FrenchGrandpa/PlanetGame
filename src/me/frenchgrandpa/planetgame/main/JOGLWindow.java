package me.frenchgrandpa.planetgame.main;

import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.media.opengl.awt.GLCanvas;

public class JOGLWindow extends Listener {
	
	public static final GLCanvas CANVAS = new GLCanvas(Main.GL_CAPABILIITIES);
	public static final String TITLE = "Block";
	public static final Frame FRAME = new Frame(TITLE);
	public static final int WIDTH = 800;
	public static final int HEIGHT = 800;

	public JOGLWindow() {
		CANVAS.addGLEventListener(new Listener.JOGLEventListener());
		CANVAS.addKeyListener(new Listener.AWTKeyListener());
		CANVAS.addMouseMotionListener(new Listener.AWTMouseMotionListener());
		CANVAS.addMouseListener(new Listener.AWTMouseListener());
		
		FRAME.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width / 2 - WIDTH / 2, Toolkit.getDefaultToolkit().getScreenSize().height / 2 - HEIGHT / 2);
		FRAME.setSize(WIDTH, HEIGHT);
		FRAME.add(CANVAS);
		FRAME.setUndecorated(true);
		FRAME.setResizable(false);
		FRAME.setVisible(true);
		FRAME.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
}
