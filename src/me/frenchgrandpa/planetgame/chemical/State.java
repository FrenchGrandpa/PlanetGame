package me.frenchgrandpa.planetgame.chemical;

public enum State {

	SOLID,
	LIQUID,
	GAS;
	
}
