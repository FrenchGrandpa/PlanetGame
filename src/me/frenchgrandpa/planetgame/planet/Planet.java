package me.frenchgrandpa.planetgame.planet;

import static me.frenchgrandpa.planetgame.main.Main.gl;

import java.awt.Color;

import javax.media.opengl.GL;

import me.frenchgrandpa.planetgame.chemical.Chemical;
import me.frenchgrandpa.planetgame.util.Vector;

public class Planet {
	
	private Vector position;
	private double radius;
	private Chemical[] chem;
	private Vector velocity;
	
	private Vector gravity = new Vector(0, 0);

	public Planet(Vector position, double radius, Chemical[] chem, Vector velocity) {
		this.position = position;
		this.radius = radius;
		this.chem = chem;
		this.velocity = velocity;
	}
	
	public void draw(boolean mode) {
		if (mode) {//if mode == true -> draw inside
			position.add(velocity);
			double x = position.getX();
			double y = position.getY();
			gl.glBegin(GL.GL_TRIANGLE_STRIP);
			double angle = 0.0;
			int points = 100;
			double amt = 0;
			
			for (int c = 0; c < chem.length; c++) {
				amt += chem[c].getAmount();
				for (int l = 0; l <= points; l++) {
					angle = 2 * Math.PI * l / points;
					Color col = chem[c].getColor();
					gl.glColor4d(col.getRed() / 255.0, col.getGreen() / 255.0, col.getBlue() / 255.0, col.getAlpha() / 255.0);
					gl.glVertex2d(x, y);
					gl.glVertex2d(x + Math.cos(angle) * radius * amt, y + Math.sin(angle) * radius * amt);
				}
			}
			gl.glEnd();
		}
		
		else {
			position.add(velocity);
			double x = position.getX();
			double y = position.getY();
			gl.glBegin(GL.GL_TRIANGLE_STRIP);
			double angle = 0.0;
			int points = 100;
			
			for (int l = 0; l <= points; l++) {
				angle = 2 * Math.PI * l / points;
				Color col = chem[chem.length - 1].getColor();
				gl.glColor4d(col.getRed() / 255.0, col.getGreen() / 255.0, col.getBlue() / 255.0, col.getAlpha() / 255.0);
				gl.glVertex2d(x, y);
				gl.glVertex2d(x + Math.cos(angle) * radius, y + Math.sin(angle) * radius);
			}
			gl.glEnd();
		}
	}
	
	public void addGravity(Planet p) {
		Vector vec = this.velocity;
		double mass = 0.0;
		double dist = Math.sqrt((p.getPosition().getX() - this.getPosition().getX()) * (p.getPosition().getX() - this.getPosition().getX()) +
								(p.getPosition().getY() - this.getPosition().getY()) * (p.getPosition().getY() - this.getPosition().getY()));	
		for (int l = 0; l < p.getChem().length; l++) {
			mass += p.getChem()[l].getWeight() * p.getChem()[l].getAmount() * p.getRadius();
		}
		mass /= 10000000;
		//System.out.println(Math.atan2(p.getPosition().getY() - this.getPosition().getY(), p.getPosition().getX() - this.getPosition().getX()));
		this.gravity.setLength(mass / (dist * dist));
		this.gravity.setAngle(Math.atan2(p.getPosition().getY() - this.getPosition().getY(), p.getPosition().getX() - this.getPosition().getX()));
		//this.velocity.setAngle(this.gravity.getAngle() - Math.PI / 2);
		this.velocity.add(this.gravity);
		//this.velocity.setLength(vec.getLength());
	}
	
	

	public Vector getPosition() {
		return position;
	}

	public void setPosition(Vector position) {
		this.position = position;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Chemical[] getChem() {
		return chem;
	}

	public void setChem(Chemical[] chem) {
		this.chem = chem;
	}

	public Vector getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector velocity) {
		this.velocity = velocity;
	}
	

}