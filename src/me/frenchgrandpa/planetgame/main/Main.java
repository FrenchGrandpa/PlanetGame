package me.frenchgrandpa.planetgame.main;

import javax.media.opengl.GL2;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.glu.GLU;

public class Main {
	
	public static final int FPS = 120;
	public static final GLProfile GL_PROFILE = GLProfile.getDefault();
	public static final GLCapabilities GL_CAPABILIITIES = new GLCapabilities(GL_PROFILE);
	
	public static boolean isRunning = true;
	
	//Initialized in Listener
	public static GL2 gl;
	public static GLU glu;
	public static Camera cam;

	public static final void main (String... args) {
		new JOGLWindow();
		new Main();
	}
	
	private Main() {
		run();
	}
	
	private void run() {
		long lastTime = System.nanoTime();
		long lastTimer = System.currentTimeMillis();
		double delta = 0;
		double nsPerTick = 1000000000 / FPS;
		int ticks = 0;
		int frames = 0;
		while (isRunning) {
			long now = System.nanoTime();
			delta += (now - lastTime) / nsPerTick;
			lastTime = now;
			boolean shouldRender = false;
			
			while (delta >= 1) {
				ticks++;
				delta -= 1;
				shouldRender = true;
			}
			
			if (shouldRender) {
				frames++;
				JOGLWindow.CANVAS.display();
			}
			
			if (System.currentTimeMillis() - lastTimer >= 1000) {
				lastTimer += 1000;
				System.out.println(ticks + " ticks, " + frames + " frames");
				JOGLWindow.FRAME.setTitle("Planet Game | " + frames + " fps");
				frames = 0;
				ticks = 0;
			}
		}
	}
}
