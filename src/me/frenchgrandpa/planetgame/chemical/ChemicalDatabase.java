package me.frenchgrandpa.planetgame.chemical;

import java.awt.Color;

public class ChemicalDatabase {
	
	public static final Chemical OXYGEN = new Chemical(1.4290, State.GAS, false, "Oxygen", "O2", new Color(9, 177, 230, 125), 0);
	public static final Chemical HYDROGEN = new Chemical(0.0899, State.GAS, true, "Hydrogen", "H2", new Color(15, 243, 230, 125), 0);
	public static final Chemical NITROGEN = new Chemical(1.2506, State.GAS, true, "Nitrogen", "N2", new Color(38, 49, 230, 125), 0);
	public static final Chemical WATER = new Chemical(999.97, State.LIQUID, false, "Water", "H2O", new Color(57, 85, 150), 0);
	public static final Chemical SILICONDIOXIDE = new Chemical(900, State.SOLID, true, "Silicon Rock", "S2O", Color.LIGHT_GRAY, 0);
															//TODO weight not correct for silicondioxide
}
//-0,3429% per +1 temperature