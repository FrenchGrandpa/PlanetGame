package me.frenchgrandpa.planetgame.main;

import static me.frenchgrandpa.planetgame.main.Main.*;

import javax.media.opengl.GL2;

public class Camera {
	
	//Standard camera vars
	public static final float STD_FOV = 70;
	public static final float STD_ASPECT = (float) JOGLWindow.WIDTH / (float) JOGLWindow.HEIGHT;
	public static final float STD_NEAR = 0.3f;
	public static final float STD_FAR = 1000;
	
	public static final float BOX = 1;

	private float x;
	private float y;
	private float z;
	
	private float rx;
	private float ry;
	private float rz;
	
	private float oldX;
	private float oldY;
	private float oldZ;
	
	public Camera() {		
		x = 1;
		y = 1;
		z = 0;
		rx = 0;
		ry = 0;
		rz = 0;
		oldX = 0;
		oldY = 0;
		oldZ = 0;
		
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(STD_FOV, STD_ASPECT, STD_NEAR, STD_FAR);
		//gl.glMatrixMode(GL2.GL_MODELVIEW);
	}
	
	public void recalibrateCamera() {
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(STD_FOV, (float) JOGLWindow.FRAME.getWidth() / (float) JOGLWindow.FRAME.getHeight(), STD_NEAR, STD_FAR);
		//gl.glMatrixMode(GL2.GL_MODELVIEW);
	}
	public void updateView() {
		gl.glRotatef(rx, 1, 0, 0);
		gl.glRotatef(ry, 0, 1, 0);
		gl.glRotatef(rz, 0, 0, 1);
		gl.glTranslatef(-x, -y, -z);
	}
	
	public void move(float amt, float dir) {
		oldX = x;
		oldZ = z;
		z -= amt * Math.sin(Math.toRadians(ry + 90 * dir));
		x -= amt * Math.cos(Math.toRadians(ry + 90 * dir));
	}
	
	public void jump(float amt) {
		oldY = y;
		y += amt;
	}
	
	public void rotateX(float amt) {
		rx+= amt;
	}
	
	public void rotateY(float amt) {
		ry+= amt;
	}
	
	public void rotateZ(float amt) {
		rz+= amt;
	}
	
	
	//getters and setters
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public float getZ() {
		return z;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public void setZ(float z) {
		this.z = z;
	}
	
	public float getRX() {
		return rx;
	}
	
	public float getRY() {
		return ry;
	}
	
	public float getRZ() {
		return rz;
	}
	
	public void setRX(float rx) {
		this.rx = rx;
	}
	
	public void setRY(float ry) {
		this.ry = ry;
	}
	
	public void setRZ(float rz) {
		this.rz = rz;
	}
	
	public float getOldX() {
		return oldX;
	}
	
	public float getOldY() {
		return oldY;
	}
	
	public float getOldZ() {
		return oldZ;
	}
}
