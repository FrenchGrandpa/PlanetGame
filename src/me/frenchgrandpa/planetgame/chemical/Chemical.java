package me.frenchgrandpa.planetgame.chemical;

import java.awt.Color;

public class Chemical {
	
	private double weight;
	private State state;
	private boolean toxic;
	private String name;
	private String formula;
	private Color color;
	private double amount;

	public Chemical(double weight, State state, boolean toxic, String name, String formula, Color color, double amount) {
		this.weight = weight;
		this.state = state;
		this.toxic = toxic;
		this.name = name;
		this.formula = formula;
		this.color = color;
		this.amount = amount;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isToxic() {
		return toxic;
	}

	public void setToxic(boolean toxic) {
		this.toxic = toxic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
}
