package me.frenchgrandpa.planetgame.planet;

import me.frenchgrandpa.planetgame.chemical.Chemical;
import me.frenchgrandpa.planetgame.chemical.ChemicalDatabase;
import me.frenchgrandpa.planetgame.util.Vector;

public class PlanetInitializer {
	
	public Planet earth, rock;

	public PlanetInitializer() {
		initEarth();
		initRock();
	}
	
	public void drawAll() {
		earth.addGravity(rock);
		earth.draw(true);
		rock.draw(true);
	}

	private void initEarth() {
		Chemical[] chems = {ChemicalDatabase.SILICONDIOXIDE, ChemicalDatabase.OXYGEN, ChemicalDatabase.NITROGEN};
		chems[0].setAmount(1.0/2.0);
		chems[1].setAmount(1.0/4.0);
		chems[2].setAmount(1.0/8.0);
		Vector vel = new Vector(0.001, 0);
		earth = new Planet(new Vector(1, 0.5), 0.1, chems, vel);
	}
	
	private void initRock() {
		Chemical[] chems = {ChemicalDatabase.SILICONDIOXIDE};
		chems[0].setAmount(1);
		Vector vel = new Vector(0, 0);
		rock = new Planet(new Vector (1, 1), 0.2, chems, vel);
	}
}
