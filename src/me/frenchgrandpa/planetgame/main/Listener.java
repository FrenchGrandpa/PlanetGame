package me.frenchgrandpa.planetgame.main;

import static me.frenchgrandpa.planetgame.main.Main.*;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

import me.frenchgrandpa.planetgame.chemical.Chemical;
import me.frenchgrandpa.planetgame.chemical.ChemicalDatabase;
import me.frenchgrandpa.planetgame.planet.Planet;
import me.frenchgrandpa.planetgame.planet.PlanetInitializer;
import me.frenchgrandpa.planetgame.util.Vector;

public class Listener {
	
	private PlanetInitializer pi;

	public class JOGLEventListener implements GLEventListener {

		@Override
		public void init(GLAutoDrawable drawable) {
			//load textures
			//Grass.texture = loadTexture(Grass.TEXTURE_PATH);
			
			//initialize variables
			gl = drawable.getGL().getGL2();
			glu = new GLU();
			gl.glEnable(GL2.GL_DEPTH_TEST);
			gl.glEnable(GL2.GL_TEXTURE_2D);
			cam = new Camera();
			cam.updateView();
			
			pi = new PlanetInitializer();
		}
		
		@Override
		public void display(GLAutoDrawable drawable) {
			//Initialize the GL environment
			gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
			gl.glLoadIdentity();
			cam.updateView();
			gl.glPushMatrix(); {
				
				pi.drawAll();
				
			} gl.glPopMatrix();
		}

		@Override
		public void dispose(GLAutoDrawable drawable) {
		}

		@Override
		public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		}
		
	}
	
	public static class AWTKeyListener implements KeyListener {
		
		public static boolean w;
		public static boolean a;
		public static boolean s;
		public static boolean d;
		public static boolean space;
		public static boolean shift;

		@Override
		public void keyPressed(KeyEvent e) {		
			switch(e.getKeyCode()) {
				case KeyEvent.VK_W: w = true; break;
				case KeyEvent.VK_A: a = true; break;
				case KeyEvent.VK_S: s = true; break;
				case KeyEvent.VK_D: d = true; break;
				case KeyEvent.VK_SPACE: space = true; break;
				case KeyEvent.VK_SHIFT: shift = true; break;
				case KeyEvent.VK_C: System.out.println(cam.getX() + " " + cam.getY() + " " + cam.getZ()); break;
				case KeyEvent.VK_ESCAPE: System.exit(0); break;
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {		
			switch(e.getKeyCode()) {
				case KeyEvent.VK_W: w = false; break;
				case KeyEvent.VK_A: a = false; break;
				case KeyEvent.VK_S: s = false; break;
				case KeyEvent.VK_D: d = false; break;
				case KeyEvent.VK_SPACE: space = false; break;
				case KeyEvent.VK_SHIFT: shift = false; break;
			}
		}

		@Override
		public void keyTyped(KeyEvent e) {		
			
		}
		
	}
	
	public static class AWTMouseMotionListener implements MouseMotionListener {

		@Override
		public void mouseMoved(MouseEvent e) {		
			//Handler.handleRotation(e);
		}

		@Override
		public void mouseDragged(MouseEvent e) {		
			mouseMoved(e);
		}
		
	}
	
	public static class AWTMouseListener implements MouseListener {
		
		public static boolean button1;
		public static boolean button3;

		@Override
		public void mouseClicked(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON1) {
				button1 = true;
			}
			else if (e.getButton() == MouseEvent.BUTTON3) {
				button3 = true;
			}
		}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON1) {
				button1 = false;
			}
			else if (e.getButton() == MouseEvent.BUTTON3) {
				button3 = false;
			}
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

	}
}
