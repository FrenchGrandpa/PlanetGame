package me.frenchgrandpa.planetgame.main;

import static me.frenchgrandpa.planetgame.main.Main.*;
import static me.frenchgrandpa.planetgame.main.Listener.AWTKeyListener.*;

import java.awt.event.MouseEvent;

public class Handler {
		
	public static final float ROTATESPEED = 1;
		
	private static int oldX = JOGLWindow.WIDTH / 2;
	private static int oldY = JOGLWindow.HEIGHT / 2;
	
	public static void handleRotation(MouseEvent e) {
		if (e != null && cam  != null) {
			if (e.getX() > oldX) {
				cam.rotateY(ROTATESPEED);
				oldX = e.getX();
			}
			if (e.getX() < oldX) {
				cam.rotateY(-ROTATESPEED);
				oldX = e.getX();
			}

			if (e.getY() > oldY) {
				cam.rotateX(ROTATESPEED);
				oldY = e.getY();
			}
			if (e.getY() < oldY) {
				cam.rotateX(-ROTATESPEED);
				oldY = e.getY();
			}
		}
	}
	
	public static void handleMovement() {
		if (cam != null) {
			if (w) {
			}
			if (a) {
			}
			if (s) {
			}
			if (d) {
			}
		}
	}
	

}
